
Parse.Cloud.define("SaveAndSendPushNotification", function(request, response) {

	//Input data
	var receiverId = request.params.receiverId;
	var senderId = request.params.senderId;
	var senderName = request.params.senderName;
	var operation = request.params.operation;
	var senderFbAndroidUId = request.params.senderFbAndroidUId;
	var statusId = null;

	if(request.params.statusId){
		statusId = request.params.statusId;
	}

	var message = getNotifMessage(senderName, operation);

    if (message == 'WRONG_OPERATION') {

    	return response.error('Failed save and send notification. Operation is wrong. '+' receiverId: '+receiverId+' senderId: '+senderId+' operation: '+operation);
    };

    //create parse object
	var SSNotification = Parse.Object.extend("SSNotification");
	var ssNotification = new SSNotification();

	ssNotification.set("receiverId", receiverId);
	ssNotification.set("senderId", senderId);
	ssNotification.set("operation", operation);
	ssNotification.set("message", message);
	ssNotification.set("senderName", senderName);
	ssNotification.set("senderFbAndroidUId", senderFbAndroidUId);
	ssNotification.set("statusId", statusId);

	// Save notification 
	ssNotification.save(null, {
	  success: function(ssNotification) {
	    
	},
	  error: function(ssNotification, error) {
	    
	   return response.error('Failed to save notification. ' + error.message);
	  }
	});

  response.success("Success SaveAndSendPushNotification");
});

function getNotifMessage(senderName, operation) {

	if(operation == 'LIKE_STATUS') {

		return senderName + ' liked your status update';
	}
	else if(operation == 'FAVORITE_STATUS') {

		return senderName + ' favourited your status update';
	}
	else if(operation == 'FOLLOW_USER') {

		return senderName + ' started following you';
	}
	else if(operation == 'MENTION_STATUS') {

		return senderName + ' mentioned you in a status update';
	}
	else {

		return 'WRONG_OPERATION';
	}
}

// this will be automatically called after successfully save of SSNotification in SaveAndSendPushNotification. This will send a push notification to user's device
Parse.Cloud.afterSave("SSNotification", function(request) {

	var query = new Parse.Query(Parse.Installation);
	query.equalTo('userId', request.object.get('receiverId'));

	Parse.Push.send({
	where: query,
	data: {
	    message: request.object.get('message'), // do not send any push with key ALERT
	    senderId: request.object.get('senderId'),
	    senderName: request.object.get('senderName'),
	    senderFbAndroidUId: request.object.get('senderFbAndroidUId'),
	    statusId: request.object.get('statusId'),
	    operation: request.object.get('operation'),
	    action: 'show_custom_push'   
	  }
	}, {
	  success: function() {
	},
	  error: function(error) {

	  	console.log('Exception in sending push notification. '+ error.message);
	}
	});
});



       